package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Rishabh
 *
 */

public class EmailValidator {
	
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = 
		    Pattern.compile("^([a-z]([a-z0-9._%+-])*){3}@([a-z0-9.-]+){3}\\.([a-z]+){2}$");

	public static boolean isValidEmail(String emailStr) {
		if(emailStr==null) {
			return false;
		}
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}
	
}
