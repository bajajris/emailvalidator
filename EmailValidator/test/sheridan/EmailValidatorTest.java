package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Rishabh
 *
 */

public class EmailValidatorTest {
	@Test
	public void testIsValidEmailFormat() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@gmail.com");
		assertTrue(isValid);
	}
	
	@Test
	public void testIsValidEmailFormatNull() {
		boolean isValid = EmailValidator.isValidEmail(null);
		assertFalse(isValid);
	}
	
	@Test
	public void testEmailValidSymbolAt() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@gmail.com");
		assertTrue(isValid);
	}
	
	@Test
	public void testEmailValidSymbolAtException() {
		boolean isValid = EmailValidator.isValidEmail("rishabh@bajaj@gmail.com");
		assertFalse(isValid);
	}
	
	@Test
	public void testEmailValidSymbolAtBoundryOut() {
		boolean isValid = EmailValidator.isValidEmail("rishabhbajajgmail.com");
		assertFalse(isValid);
	}
	
	@Test
	public void testIsValidEmailAcount() {
		boolean isValid = EmailValidator.isValidEmail("rishabh@gmail.com");
		assertTrue(isValid);
	}
	
	@Test
	public void testIsValidEmailAcountExceptionNumber() {
		boolean isValid = EmailValidator.isValidEmail("222rishabh@gmail.com");
		assertFalse(isValid);
	}
	
	@Test
	public void testIsValidEmailAcountExceptionUppercase() {
		boolean isValid = EmailValidator.isValidEmail("Rishabh@gmail.com");
		assertFalse(isValid);
	}
	
	@Test
	public void testIsValidEmailAcountBoundryIn() {
		boolean isValid = EmailValidator.isValidEmail("rrr@gmail.com");
		assertTrue(isValid);
	}
	
	@Test
	public void testIsValidEmailAcountBoundryOut() {
		boolean isValid = EmailValidator.isValidEmail("rr@gmail.com");
		assertFalse(isValid);
	}
	
	@Test
	public void testIsValidEmailDomain() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@gmail.com");
		assertTrue(isValid);
	}
	
	@Test
	public void testIsValidEmailDomainException() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@.com");
		assertFalse(isValid);
	}
	
	@Test
	public void testIsValidEmailDomainBoundryIn() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@abc.com");
		assertTrue(isValid);
	}
	
	@Test
	public void testIsValidEmailDomainBoundryOut() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@ab.com");
		assertFalse(isValid);
	}
	
	@Test
	public void testIsValidEmailExtension() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@gmail.com");
		assertTrue(isValid);
	}
	
	@Test
	public void testIsValidEmailExtensionException() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@gmail.");
		assertFalse(isValid);
	}
	
	@Test
	public void testIsValidEmailExtensionBoundryIn() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@gmail.ca");
		assertTrue(isValid);
	}
	

	@Test
	public void testIsValidEmailExtensionBoundryOut() {
		boolean isValid = EmailValidator.isValidEmail("rishabh.bajaj@gmail.c");
		assertFalse(isValid);
	}
}
